import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function GET(req, res) {
	const { searchParams } = new URL(req.url);
	const useremail = searchParams.get("useremail");

	try {
		const user = await prisma.user.findUnique({
			where: {
				email: useremail,
			},
		});
		return NextResponse.json({ user }, { status: 200 });
	} catch (error) {
		console.error(error);
		return NextResponse.json(
			{ message: "Failed to get user" },
			{ status: 500 }
		);
	} finally {
		await prisma.$disconnect();
	}
}

// Update profile page (bio and course)
export async function PUT(req, res) {
	const { bio, course, id } = await req.json();
	try {
		const updatedUser = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				biography: bio,
				course: course,
			},
		});

		return NextResponse.json(
			{ message: "Successfully updated profile!", user: updatedUser },

			{ status: 200 }
		);
	} catch (error) {
		console.error(error);
		return NextResponse.json(
			{ message: "Failed to updated profile" },
			{ status: 500 }
		);
	} finally {
		await prisma.$disconnect();
	}
}
