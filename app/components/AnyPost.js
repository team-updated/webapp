import Image from "next/image";
import Link from "next/link";

function AnyPost({ content, id, postedBy, postedWith, createdAt }) {
	return (
		<>
			<div className="bg-white border border-gray-200 p-2 my-2">
				<div className="flex flex-row gap-2 items-center mt-2 mb-4">
					<Link href={"/home"}>
						{postedBy !== null ? (
							<Image
								src={postedBy.image}
								alt={`Profile picture of ${postedBy.name}`}
								className="rounded-full"
								style={{ minWidth: "32px" }}
								width={44}
								height={44}
							/>
						) : postedWith !== null ? (
							<Image
								src={postedWith.image}
								alt={`Profile picture of ${postedWith.name}`}
								className="rounded-full"
								style={{ minWidth: "32px" }}
								width={44}
								height={44}
							/>
						) : (
							<Image
								src={"/images/upang.webp"}
								alt={`Default image for poster`}
								className="rounded-full"
								style={{ minWidth: "32px" }}
								width={44}
								height={44}
							/>
						)}
					</Link>
					<div>
						<Link
							href={"/home"}
							className="font-semibold hover:underline active:text-green-700"
						>
							{postedBy.name || postedWith.name}
						</Link>
						<p className="text-xs">
							{createdAt.slice(0, 10)} at {createdAt.slice(11, 16)} UTC
						</p>
					</div>
				</div>
				<p className="text-sm">{content}</p>
				{/* {post?.images && (
					<Image
						src={post_placeholder.images.img_src}
						alt={post_placeholder.images.img_alt}
						height={500}
						width={500}
						className="my-4"
						style={{ width: "100%", height: "auto" }}
					/>
				)} */}
				<div className="grid grid-cols-2 mt-2 bg-gray-200 text-xs xs:text-sm sm:text-base">
					<button className="p-1 hover:bg-gray-300 active:text-green-700">
						<span className="material-symbols-outlined align-middle size-20 mr-2 text-sky-500">
							thumb_up
						</span>
						Like
					</button>
					<button className="p-1 hover:bg-gray-300 active:text-green-700">
						<span className="material-symbols-outlined align-middle size-20 mr-2 text-sky-500">
							comment
						</span>
						Comment
					</button>
				</div>
			</div>
		</>
	);
}

export default AnyPost;
