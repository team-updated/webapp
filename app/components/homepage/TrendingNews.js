import Link from "next/link";

function TrendingNews() {
	return (
		<>
			<h2 className="font-bold rounded py-1 px-2 bg-gray-300 my-2">
				<span className="material-symbols-outlined size-24 align-bottom mr-2 text-green-500">
					trending_up
				</span>
				Trending News
			</h2>
			<ul>
				<li>
					<Link
						href={"/home"}
						className="my-2 block hover:bg-gray-200 py-1 px-2 active:text-green-700 rounded"
					>
						<p className="font-semibold">news-1</p>
						<p className="text-xs text-neutral-700">1 year ago</p>
					</Link>
				</li>
				<li>
					<Link
						href={"/home"}
						className="my-2 block hover:bg-gray-200 py-1 px-2 active:text-green-700 rounded"
					>
						<p className="font-semibold">news-2</p>
						<p className="text-xs text-neutral-700">1 year ago</p>
					</Link>
				</li>
				<li>
					<Link
						href={"/home"}
						className="my-2 block hover:bg-gray-200 py-1 px-2 active:text-green-700 rounded"
					>
						<p className="font-semibold">news-3</p>
						<p className="text-xs text-neutral-700">1 year ago</p>
					</Link>
				</li>
			</ul>
		</>
	);
}

export default TrendingNews;
