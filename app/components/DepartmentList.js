"use client";

import Link from "next/link";
import Image from "next/image";

function DepartmentList({ departmentList }) {
	return (
		<ul className={`${departmentList.length > 0 ? "my-4" : "my-0"}`}>
			{departmentList.map((department) => {
				const { id, name, description, image } = department;

				return (
					<li
						key={id}
						className="py-2 px-4 bg-gray-300 block rounded my-4"
					>
						<div className="flex flex-row gap-2 items-center justify-start my-4">
							{/* <Link
								href={"/"}
								className="block"
							>
								<Image
									src={image}
									alt="Department logo"
									width={140}
									height={140}
									className="rounded-full w-16"
									style={{ minWidth: "48px" }}
								/>
							</Link> */}
							<Link
								href={`/departments/department?id=${id}`}
								className="font-bold text-lg block hover:underline active:text-green-700"
							>
								{name}
							</Link>
						</div>
						<hr
							className={`${description ? "block" : "hidden"} border-gray-500`}
						/>
						<p className="my-2">{description}</p>
					</li>
				);
			})}
		</ul>
	);
}

export default DepartmentList;
