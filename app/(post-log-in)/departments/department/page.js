"use client";

import { useSearchParams } from "next/navigation";
import { useState, useEffect } from "react";
import Department from "@/app/components/Department";

function DepartmentPage() {
	const searchParams = useSearchParams();
	const id = searchParams.get("id");

	const [currentDepartment, setCurrentDepartment] = useState(null);

	useEffect(() => {
		async function getDepartment() {
			try {
				const res = await fetch(
					`/api/departments/department?department_id=${id}`
				);
				if (res.ok) {
					const data = await res.json();
					setCurrentDepartment(data.department);
				}
			} catch (error) {
				console.error(error);
			}
		}
		getDepartment();
	}, []);

	if (currentDepartment) {
		return (
			<>
				<Department
					department_id={currentDepartment.id}
					department_desc={currentDepartment.description}
					department_name={currentDepartment.name}
					admin={currentDepartment.admin}
					members={currentDepartment.members}
					image={currentDepartment.image}
				/>
			</>
		);
	}
}

export default DepartmentPage;
