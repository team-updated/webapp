import LeftColumn from "./left-column/LeftColumn";
import RightColumn from "./right-column/RightColumn";

function Header() {
	return (
		<header className="z-50 sticky top-0 py-2 px-3 sm:px-8 bg-green-600 flex flex-row gap-3 justify-between items-center">
			<LeftColumn />
			<RightColumn />
		</header>
	);
}

export default Header;
