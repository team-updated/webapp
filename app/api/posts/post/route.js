import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function POST(req, res) {
	const { content, postedBy } = await req.json();
	try {
		const newPost = await prisma.post.create({
			include: {
				postedBy: true,
			},
			data: {
				content: content,
				postedBy: {
					connect: { id: postedBy },
				},
			},
		});
		return NextResponse.json({ newPost: newPost }, { status: 200 });
	} catch (error) {
		console.error("Error creating post:", error);
		return NextResponse.json(
			{ error: "Failed to create post." },
			{ status: 500 }
		);
	} finally {
		await prisma.$disconnect();
	}
}
