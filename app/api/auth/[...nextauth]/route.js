import { PrismaClient } from "@prisma/client";
import NextAuth from "next-auth/next";
import GoogleProvider from "next-auth/providers/google";

const prisma = new PrismaClient();

const handler = NextAuth({
	providers: [
		GoogleProvider({
			clientId: process.env.GOOGLE_CLIENT_ID,
			clientSecret: process.env.GOOGLE_CLIENT_SECRET,
		}),
	],
	pages: {
		error: "/access-denied",
	},

	callbacks: {
		async signIn({ user }) {
			 // check if email domain is valid, deny access if not
			 const emailDomain = user.email.split("@")[1];

			 if (emailDomain !== "phinmaed.com") {
			 	return false;
			 } else {
			 	// if email domain is valid, check if a user with the same email
			 	// already exist in the database,
			 	// if it doesn't, create a new user
			const existingUser = await prisma.user.findUnique({
				where: {
					email: user.email,
				},
			});
			if (!existingUser) {
				try {
					await prisma.user.create({
						data: {
							email: user.email,
							name: user.name,
							image: user.image,
						},
					});
				} catch (error) {
					console.error(error);
				} finally {
					await prisma.$disconnect();
				}
			}
			return true;
			 }
		},
	},
	secret: process.env.NEXTAUTH_SECRET,
});

export { handler as GET, handler as POST };

