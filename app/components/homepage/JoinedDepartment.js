import Link from "next/link";
import Image from "next/image";

function JoinedDepartment({
	department_name,
	department_img_src,
	department_img_alt,
}) {
	return (
		<>
			<h2 className="font-bold rounded py-1 px-2 bg-gray-300 my-2">
				<span className="material-symbols-outlined align-bottom size-24 mr-2 text-sky-500">
					groups
				</span>
				Your Department
			</h2>

			<Link
				href={"/"}
				className="py-1 px-2 my-2 hover:bg-gray-200 active:text-green-700 flex flex-row gap-2 items-center"
			>
				<Image
					src={department_img_src}
					alt={department_img_alt}
					width={60}
					height={60}
					className="rounded-full"
					style={{ minWidth: "32px" }}
				/>
				{department_name}
			</Link>
		</>
	);
}

export default JoinedDepartment;
