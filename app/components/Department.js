"use client";

import Image from "next/image";
import Link from "next/link";
import { useState, useEffect, useRef } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";

function Department({
	department_id,
	department_desc,
	department_name,
	members,
	admin,
	image,
}) {
	const router = useRouter();
	const { data: session } = useSession();
	const [currentUser, setCurrentUser] = useState(null);
	const [currentDeptMembers, setCurrentDeptMembers] = useState(members);
	const [currentAdmin, setCurrentAdmin] = useState(admin);
	const [isMdodalOpen, setIsModalOpen] = useState(false);
	const [action, setAction] = useState("");
	const [isMember, setIsMember] = useState(false);
	const [transferTo, setTransferTo] = useState("");
	const [selected, setSelected] = useState("");
	const [postContent, setPostContent] = useState("");

	const actionConfirmationRef = useRef(null);
	const membersDropdownRef = useRef(null);

	useEffect(() => {
		async function getUser() {
			const res = await fetch(
				`/api/users/user?useremail=${session?.user.email}`
			);
			const data = await res.json();
			setCurrentUser(data?.user);
		}
		getUser();
	}, [session]);

	useEffect(() => {
		setIsMember(() =>
			currentDeptMembers.some((member) => {
				return member.id === currentUser?.id;
			})
		);
	}, [currentDeptMembers, currentUser]);

	// compare currentUser.id to admin.id to determine the role of the page's visitor
	// if comparison === true, show elements that are only available to admins

	const isAdmin = currentAdmin.id === currentUser?.id;

	// check if department id and user id is present
	// disable join/leave button if false
	const isIdReady =
		department_id !== ("" || null || undefined) &&
		currentUser?.id !== ("" || null || undefined);

	useEffect(() => {
		if (isMdodalOpen) {
			document.body.style.overflow = "hidden";
		} else {
			document.body.style.overflow = "auto";
		}
	}, [isMdodalOpen]);

	const confirmActionHandle = async () => {
		async function updateDepartmentMembers() {
			const res = await fetch(`/api/departments/department/${action}`, {
				method: "PUT",
				headers: { "Content-type": "application/json" },
				body: JSON.stringify({
					departmentId: department_id,
					userId: currentUser.id,
				}),
			});
			if (res.ok && action === "join") {
				const data = await res.json();
				setCurrentDeptMembers(data.updatedMembers.members);
			} else if (res.ok && action === "leave") {
				router.push("/departments");
			}
		}
		updateDepartmentMembers();
		actionConfirmationRef.current.close();
		setIsModalOpen(false);
	};

	const confirmTransferHandle = () => {
		async function updateAdmin() {
			const res = await fetch(
				`/api/departments/department/transfer-adminship`,
				{
					method: "PUT",
					headers: { "Content-type": "application/json" },
					body: JSON.stringify({
						newAdminId: transferTo,
						departmentId: department_id,
					}),
				}
			);
			if (res.ok) {
				const data = await res.json();
				setCurrentAdmin(data.updatedDepartment.admin);
				setCurrentDeptMembers(data.updatedDepartment.members);
			}
		}
		updateAdmin();
		setTransferTo("");
		setSelected("");
		membersDropdownRef.current.close();
	};

	const createPostHandle = () => {};

	if (currentUser && currentDeptMembers) {
		return (
			<>
				<main>
					<div className="w-full sm:w-3/4 xl:w-1/2 mx-auto p-4 bg-white">
						<section className="flex flex-col items-center gap-4 justify-center bg-gray-200 p-2 rounded my-4">
							{image ? (
								<Image
									src={logo_src}
									alt={logo_alt}
									width={100}
									height={100}
									className="rounded-full"
									style={{ minWidth: "44px" }}
								/>
							) : (
								<Image
									src={"/images/default_user_img.webp"}
									alt={"deafult image for user"}
									width={100}
									height={100}
									className="rounded-full"
									style={{ minWidth: "44px" }}
								/>
							)}
							<div className=" text-center sm:text-left">
								<h1 className="text-lg font-bold">{department_name}</h1>
							</div>
						</section>
						<div
							className={`flex flex-row ${
								isAdmin ? "justify-between" : "justify-end"
							}  gap-2 py-2  ml-auto`}
						>
							<button
								onClick={() => {
									membersDropdownRef.current.showModal();
									console.log("clicked", isAdmin);
								}}
								className={`${
									isAdmin ? "block" : "hidden"
								} bg-gray-200 p-2 rounded text-xs hover:bg-green-500 hover:text-neutral-200  active:text-neutral-400 active:bg-green-600`}
							>
								Transfer Adminship
							</button>
							{isMember ? (
								<button
									onClick={() => {
										if (currentDeptMembers.length > 1 && isAdmin) {
											window.alert("Transfer adminship before leaving.");
										} else {
											actionConfirmationRef.current.showModal();
											setIsModalOpen(true);
											setAction("leave");
										}
									}}
									disabled={!isIdReady}
									className="bg-gray-200 p-2 rounded text-xs hover:bg-rose-500 hover:text-neutral-200  active:text-neutral-400 active:bg-rose-600"
								>
									Leave Department
								</button>
							) : (
								<button
									disabled={!isIdReady}
									onClick={() => {
										actionConfirmationRef.current.showModal();
										setIsModalOpen(true);
										setAction("join");
									}}
									className="bg-gray-200 p-2 rounded text-xs hover:bg-green-500 hover:text-neutral-200  active:text-neutral-400 active:bg-green-600"
								>
									Join Department
								</button>
							)}
						</div>
						<dialog
							ref={membersDropdownRef}
							className="bg-gray-200 p-2 rounded border border-gray-800"
						>
							<p className="text-center m-2 text-lg font-semibold">
								Transfer Adminship
							</p>
							<ul>
								{currentDeptMembers
									.filter((member) => {
										return member.id !== admin.id;
									})
									.map((member) => {
										return (
											<li
												key={member.id}
												className={`${
													selected === member.id
														? "bg-green-500"
														: "bg-gray-300"
												}  rounded my-4 w-full`}
											>
												<button
													onClick={() => {
														setTransferTo(member.id);
														setSelected(member.id);
													}}
													className={` flex flex-col gap-1 p-2`}
												>
													<p className="text-sm">{member.name} </p>
													<p className="text-gray-600 text-xs">
														{member.email}
													</p>
												</button>
											</li>
										);
									})}
							</ul>
							<div className="flex flex-row gap-2 justify-center items-center">
								<button
									onClick={() => {
										setTransferTo("");
										setSelected("");
										membersDropdownRef.current.close();
									}}
									className="bg-neutral-300 active:bg-rose-400 text-sm p-2 rounded"
								>
									Cancel
								</button>
								<button
									onClick={confirmTransferHandle}
									className="bg-green-500 active:bg-green-600 text-sm p-2 rounded text-neutral-200"
								>
									Transfer
								</button>
							</div>
						</dialog>
						<dialog
							className="bg-gray-200 p-2 rounded border border-gray-800"
							ref={actionConfirmationRef}
						>
							<p className="text-center m-2 text-lg font-semibold">
								Confirm action?
							</p>
							<div className="flex flex-row gap-2 items-center justify-center m-2">
								<button
									onClick={() => {
										actionConfirmationRef.current.close();
										setIsModalOpen(false);
										setAction("");
									}}
									className="bg-neutral-300 active:bg-rose-400 text-sm p-2 rounded "
								>
									Cancel
								</button>
								<button
									onClick={confirmActionHandle}
									className="bg-green-500 active:bg-green-600 text-sm p-2 rounded text-neutral-200"
								>
									Confirm
								</button>
							</div>
						</dialog>
						{isAdmin && (
							<form
								onSubmit={(e) => {
									e.preventDefault();
									if (postContent === "") {
										return;
									} else {
										createPostHandle();
									}
									e.target.reset();
									setPostContent("");
								}}
								className="w-full my-0 xs:my-4 border border-gray-400 rounded"
							>
								<div className="flex flex-row gap-2 items-center p-2">
									{image ? (
										<Image
											src={image}
											alt={`Logo of ${department_name}`}
											height={28}
											width={28}
											className="rounded-full"
										/>
									) : (
										<Image
											src={"/images/default_user_img.webp"}
											alt="Default image for user"
											height={28}
											width={28}
											className="rounded-full"
										/>
									)}
									<textarea
										onChange={(e) => {
											setPostContent(e.target.value);
										}}
										className="bg-gray-100 rounded px-2 py-1 text-sm flex-grow resize-none h-20 outline-none"
										placeholder="Write a post"
									/>
								</div>
								<div className="grid grid-cols-2 border-t border-gray-400 text-xs">
									<label
										title="Photo"
										className="p-1 border-r text-center cursor-pointer border-gray-400"
									>
										<span className="p-1 material-symbols-outlined align-middle size-20 mr-1 text-emerald-500">
											image
										</span>
										<span className="hidden xs:inline">Photo</span>
										<input
											hidden
											type="file"
											accept="image/png, image/webp, image/jpeg, image/jpg"
										/>
									</label>

									<button
										type="submit"
										className="p-1 bg-green-600 text-neutral-50"
									>
										Create Post
									</button>
								</div>
							</form>
						)}
						<section className="bg-gray-200 my-4 rounded p-2">
							<h2 className="text-lg font-bold">Description</h2>
							<p className={`${department_desc ? "my-2" : "my-0"}`}>
								{department_desc}
							</p>
						</section>
						<section className="bg-gray-200 my-4 rounded p-2">
							<h2 className="text-lg font-bold">Admin</h2>
							<Link
								href="/home"
								className="flex flex-row gap-2 items-center my-2"
							>
								{currentAdmin?.image ? (
									<Image
										height={80}
										width={80}
										style={{ maxWidth: "30px", minWidth: "24px" }}
										className="rounded-full block"
										src={currentAdmin.image}
										alt={`Profile picture of ${currentAdmin?.name}`}
									/>
								) : (
									<Image
										height={80}
										width={80}
										style={{ maxWidth: "30px", minWidth: "24px" }}
										className="rounded-full block"
										src={"/images/public_user_img.webp"}
										alt="Default image of users"
									/>
								)}

								<p>{currentAdmin.name} </p>
							</Link>
						</section>

						<section className="bg-gray-200 my-4 rounded p-2">
							<h2 className="text-lg font-bold">Members</h2>

							<ul
								className={`${
									currentDeptMembers.filter((member) => {
										return member.id !== currentAdmin.id;
									}).length > 0
										? "my-2"
										: "my-0"
								}`}
							>
								{currentDeptMembers
									.filter((member) => {
										return member.id !== currentAdmin.id;
									})
									.map((member) => {
										return (
											<li key={member.id}>
												<Link
													href="/home"
													className="flex flex-row gap-2 items-center my-2"
												>
													{member?.image ? (
														<Image
															height={80}
															width={80}
															style={{ maxWidth: "30px", minWidth: "24px" }}
															className="rounded-full block"
															src={member.image}
															alt={`Profile picture of ${member?.name}`}
														/>
													) : (
														<Image
															height={80}
															width={80}
															style={{ maxWidth: "30px", minWidth: "24px" }}
															className="rounded-full block"
															src={"/images/public_user_img.webp"}
															alt="Default image of users"
														/>
													)}

													<p>{member.name} </p>
												</Link>
											</li>
										);
									})}
							</ul>
						</section>
						{/* content of post container will be replaced with post that matches the profiles id */}
						<div className="bg-gray-200 my-4"></div>
					</div>
				</main>
			</>
		);
	}
}

export default Department;
