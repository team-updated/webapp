import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function GET(req, res) {
	try {
		const departments = await prisma.department.findMany();
		return NextResponse.json({ departments }, { status: 200 });
	} catch (error) {
		console.error(error);
		return NextResponse.json({ message: "Failed to get departments" });
	} finally {
		await prisma.$disconnect();
	}
}
