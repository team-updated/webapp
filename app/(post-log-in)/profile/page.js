"use client";

import Link from "next/link";
import Image from "next/image";
import { useSession } from "next-auth/react";
import SiginReminder from "@/app/components/SigninReminder";
import { useState, useEffect, useRef } from "react";

function Profile() {
	const { data: session } = useSession();

	const [newBio, setNewBio] = useState("");
	const [newCourse, setNewCourse] = useState("");
	const [currentUser, setCurrentUser] = useState(null);
	const [isMdodalOpen, setIsModalOpen] = useState(false);

	const editProfileDialogRef = useRef(null);
	const editProfileFormRef = useRef(null);
	const bioInputRef = useRef(null);
	const courseInputRef = useRef(null);

	const sessionStatus = session !== null;
	const userStatus = currentUser !== null || undefined;

	useEffect(() => {
		console.log(userStatus, "user status: ");
	}, [userStatus]);

	const updateDataHandle = async (e) => {
		e.preventDefault();
		const requestBody = {
			bio: newBio,
			course: newCourse,
			id: currentUser?.id,
		};

		try {
			const res = await fetch(`/api/users/user`, {
				method: "PUT",
				headers: { "Content-type": "application/json" },
				body: JSON.stringify(requestBody),
			});
			if (res.ok) {
				const data = await res.json();
				console.log(data);
				setCurrentUser(data.user);
			} else {
				throw new Error("Failed to update profile");
			}
		} catch (error) {
			console.error(error);
		}
		e.target.reset();
		editProfileDialogRef.current.close();
		setIsModalOpen(false);
	};

	// lock scroll when modal is open
	useEffect(() => {
		if (isMdodalOpen) {
			document.body.style.overflow = "hidden";
		} else {
			document.body.style.overflow = "auto";
		}
	}, [isMdodalOpen]);

	// get current user
	useEffect(() => {
		async function getUser() {
			const res = await fetch(
				`/api/users/user?useremail=${session?.user.email}`
			);
			const data = await res.json();

			setCurrentUser(data?.user);
		}
		getUser();
	}, [session]);

	return (
		<>
			{sessionStatus && (
				<main className="bg-gray-100">
					<div className="w-full sm:w-3/4 xl:w-1/2 mx-auto p-4 bg-white">
						<section className="flex flex-col sm:flex-row items-center gap-4 justify-center bg-gray-200 p-2 rounded my-4">
							{session?.user ? (
								<Image
									src={session.user.image}
									alt={`Profile of ${session.user.name}`}
									width={80}
									height={80}
									className="rounded-full"
								/>
							) : (
								<Image
									src={"/images/default_user_img.webp"}
									alt="default user image"
									width={80}
									height={80}
									className="rounded-full"
								/>
							)}
							<div className=" text-center sm:text-left">
								<h1 className="text-lg font-bold">{session?.user.name}</h1>
							</div>
						</section>

						<section className="bg-gray-200 my-4 p-4 rounded">
							<div className="flex flex-row items-center justify-between gap-2">
								<h2 className="text-lg font-bold">About</h2>
								<button
									disabled={!userStatus}
									onClick={() => {
										editProfileDialogRef.current.showModal();
										bioInputRef.current.value = currentUser?.biography;
										courseInputRef.current.value = currentUser?.course;
										setIsModalOpen(true);
									}}
								>
									<span className="material-symbols-outlined align-middle size-24">
										edit
									</span>
								</button>
								<dialog
									ref={editProfileDialogRef}
									className="rounded border-2 border-gray-400 w-4/5 sm:w-3/5 sm:lg:w-1/2"
								>
									<form
										ref={editProfileFormRef}
										onSubmit={updateDataHandle}
										className="bg-gray-100 p-4 flex flex-col items-center justify-center gap-2 "
									>
										<textarea
											name="biography"
											ref={bioInputRef}
											required
											className="bg-gray-100 border border-gray-400 rounded resize-none w-full h-40 p-2 text-sm"
											type="text"
											placeholder="Your biography"
											onChange={(e) => {
												setNewBio(e.target.value);
											}}
										></textarea>
										<input
											name="course"
											ref={courseInputRef}
											required
											className="bg-gray-100 border border-gray-400 rounded w-full p-2 my-2"
											type="text"
											max={100}
											placeholder="Your course"
											onChange={(e) => {
												setNewCourse(e.target.value);
											}}
										/>
										<fieldset className="text-sm flex flex-row justify-center items-center gap-2">
											<button
												onClick={() => {
													editProfileDialogRef.current.close();
													editProfileFormRef.current.reset();
													setIsModalOpen(false);
												}}
												className="p-2 bg-gray-300 rounded active:bg-rose-400 active:text-neutral-200"
											>
												Cancel
											</button>
											<button
												className="p-2 bg-gray-300 rounded active:bg-green-500 active:neutral-text-200"
												type="submit"
											>
												Update
											</button>
										</fieldset>
									</form>
								</dialog>
							</div>

							<div className="my-4">
								<h3 className="font-semibold">Biography</h3>
								<p className="my-2">{currentUser?.biography}</p>
							</div>
							<div className="my-4">
								<h3 className="font-semibold">Course</h3>
								<p>{currentUser?.course}</p>
							</div>
							<div className="my-4">
								<h3 className="font-semibold">Department</h3>
								<Link href="/home">{currentUser?.department}</Link>
							</div>
						</section>
						{/* content of post container will be replaced with post that matches the profiles id */}
						<div className="bg-gray-200 my-4"></div>
					</div>
				</main>
			)}
			{!session && <SiginReminder />}
		</>
	);
}

export default Profile;
