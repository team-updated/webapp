import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function POST(req, res) {
	const { content, departmentId } = await req.json();
	try {
		const newPost = await prisma.departmentPost.create({
			include: {
				postedWith: true,
			},
			data: {
				content: content,
				postedWith: {
					connect: { id: departmentId },
				},
			},
		});
		return NextResponse.json({ newPost: newPost }, { status: 200 });
	} catch (error) {
		console.error("Error creating department post:", error);
		return NextResponse.json(
			{ error: "Failed to create department post." },
			{ status: 500 }
		);
	} finally {
		await prisma.$disconnect();
	}
}
