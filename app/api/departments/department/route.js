import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function GET(req, res) {
	const { searchParams } = new URL(req.url);
	const departmentId = searchParams.get("department_id");
	try {
		const department = await prisma.department.findUnique({
			where: {
				id: departmentId,
			},
			include: {
				members: true,
				admin: true,
			},
		});
		return NextResponse.json({ department }, { status: 200 });
	} catch (error) {
		console.error(error);
		return NextResponse.json(
			{
				message: "Failed to get department",
			},
			{ status: 500 }
		);
	} finally {
		await prisma.$disconnect();
	}
}

export async function POST(req, res) {
	const { departmentName, departmentDescription, adminId } = await req.json();
	const memberIds = [adminId];

	try {
		if (!departmentName || !adminId) {
			return NextResponse.json(
				{
					error: "Invalid data. Both departmentName and adminId are required.",
				},
				{ status: 400 }
			);
		}

		const newDepartment = await prisma.department.create({
			include: {
				members: true,
				admin: true,
			},
			data: {
				name: departmentName,
				description: departmentDescription,
				admin: {
					connect: { id: adminId },
				},
				members: {
					connect: memberIds.map((id) => ({ id })),
				},
			},
		});

		return NextResponse.json(
			{ message: "Department Created!", newDepartment: newDepartment },
			{ status: 201 }
		);
	} catch (error) {
		console.error("Error creating department:", error);
		return NextResponse.json(
			{ error: "Failed to create department." },
			{ status: 500 }
		);
	} finally {
		await prisma.$disconnect();
	}
}
