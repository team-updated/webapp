"use client";

import Image from "next/image";
import Link from "next/link";
import { useState, useRef, useEffect } from "react";

function LeftColumn() {
	const [mobileSearchbarVisible, setMobileSearchbarVisible] = useState(false);
	const [searchbarInput, setSearchbarInput] = useState("");
	const [searchResultVisible, setSearchResultVisible] = useState(false);
	const [mobileSearchbarInput, setMobileSearchbarInput] = useState("");
	const [mobileSearchResultVisible, setMobileSearchResultVisible] =
		useState(false);

	const fieldsetRef = useRef(null);
	const searchbarRef = useRef(null);
	const mobileSearchbarRef = useRef(null);

	useEffect(() => {}, []);

	useEffect(() => {
		const handleClickOutside = (event) => {
			if (!fieldsetRef.current.contains(event.target)) {
				setMobileSearchbarVisible(false);
				mobileSearchbarRef.current.value = "";
			}
		};
		if (mobileSearchbarVisible) {
			document.addEventListener("mousedown", handleClickOutside);
		} else {
			document.removeEventListener("mousedown", handleClickOutside);
		}

		return () => {
			document.removeEventListener("mousedown", handleClickOutside);
		};
	}, [mobileSearchbarVisible]);

	useEffect(() => {
		const handleClickOutside = (event) => {
			if (!fieldsetRef.current.contains(event.target)) {
				setSearchResultVisible(false);
				searchbarRef.current.value = "";
			}
		};
		if (searchResultVisible) {
			document.addEventListener("mousedown", handleClickOutside);
		} else {
			document.removeEventListener("mousedown", handleClickOutside);
		}
		return () => {
			document.removeEventListener("mousedown", handleClickOutside);
		};
	}, [searchResultVisible]);

	return (
		<div className="flex flex-row items-center gap-2 xs:gap-3">
			<Link href={"/home"}>
				<Image
					src={"/logo/pn_logo_medium.webp"}
					alt="Phinma News Logo"
					width={50}
					height={40}
					style={{ minWidth: "50px" }}
				/>
			</Link>
			<fieldset
				ref={fieldsetRef}
				className="bg-neutral-50 flex flex-row items-center rounded-full border static xs:relative"
			>
				<label
					onClick={() => {
						setMobileSearchbarVisible((prevState) => !prevState);
					}}
					className="cursor-pointer text-neutral-700 rounded-full active:text-neutral-800"
					htmlFor="searchbar"
				>
					<span className="material-symbols-outlined size-24 wght-300 align-bottom py-2 px-2">
						search
					</span>
				</label>
				{/* search bar */}
				<input
					ref={searchbarRef}
					onChange={(e) => {
						setSearchbarInput(e.target.value);
						if (e.target.value !== "") {
							setSearchResultVisible(true);
						} else {
							setSearchResultVisible(false);
						}
					}}
					className="py-2 pr-4 outline-none rounded-r-full hidden xs:block xs:w-32 sm:w-48 text-sm bg-neutral-50"
					placeholder="Search"
					id="searchbar"
					type="search"
				/>
				<div
					className={`${searchResultVisible ? "xs:block" : "xs:hidden"} hidden`}
				>
					<div
						className={`absolute ${
							searchbarInput.length === 0 ? "hidden" : "block"
						} bg-black text-white p-2 top-12 left-0 right-0 rounded`}
					>
						search result
					</div>
				</div>
				{/* mobile search bar */}
				<div
					className={`${
						mobileSearchbarVisible ? "block" : "hidden"
					} xs:hidden absolute left-0 right-0 top-14 bg-neutral-800 p-4`}
				>
					<input
						onChange={(e) => {
							setMobileSearchbarInput(e.target.value);
							if (e.target.value !== "") {
								setMobileSearchResultVisible(true);
							} else {
								setMobileSearchResultVisible(false);
							}
						}}
						ref={mobileSearchbarRef}
						placeholder="search"
						type="search"
						className={` bg-neutral-50 text-sm w-full text-neutral-600 rounded-full py-2 px-4 outline-none`}
					/>
					<div
						className={`${
							mobileSearchResultVisible ? "block" : "hidden"
						} bg-black text-white p-2 mt-4`}
					>
						search result mobile
					</div>
				</div>
			</fieldset>
		</div>
	);
}

export default LeftColumn;
